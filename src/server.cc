// -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
// Small example how write text.
//
// This code is public domain
// (but note, that the led-matrix library this depends on is GPL v2)
// This is based on rpi-rgb-led-matrix/examples-api-use/text-example.cc

#include "../lib/utf8-internal.h"
#include "graphics.h"
#include "led-matrix.h"

#include <arpa/inet.h>
#include <getopt.h>
#include <math.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#define PORT 8080

using namespace rgb_matrix;

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options]\n", progname);
  fprintf(stderr, "Starts a UDP server and reads from port 8080"
                  "Empty string: clear screen\n");
  fprintf(stderr, "Options:\n");
  fprintf(
      stderr,
      "\t-f <font-file>    : Use given font.\n"
      "\t-x <x-origin>     : X-Origin of displaying text (Default: 0)\n"
      "\t-y <y-origin>     : Y-Origin of displaying text (Default: 0)\n"
      "\t-S <spacing>      : Spacing pixels between letters (Default: 0)\n"
      "\t-C <r,g,b>        : Color. Default 255,255,0\n"
      "\t-B <r,g,b>        : Font Background-Color. Default 0,0,0\n"
      "\t-O <r,g,b>        : Outline-Color, e.g. to increase contrast.\n"
      "\t-F <r,g,b>        : Panel flooding-background color. Default 0,0,0\n"
      "\n");
  rgb_matrix::PrintMatrixFlags(stderr);
  return 1;
}

static bool parseColor(Color *c, const char *str) {
  return sscanf(str, "%hhu,%hhu,%hhu", &c->r, &c->g, &c->b) == 3;
}

static bool FullSaturation(const Color &c) {
  return (c.r == 0 || c.r == 255) && (c.g == 0 || c.g == 255) &&
         (c.b == 0 || c.b == 255);
}

int StopFlag = 1;

void termHandler(int i) {
  printf("INFO Server shutdown...");
  StopFlag = 0;
  sleep(3);
  printf("INFO Server stopped... OK");

  exit(EXIT_SUCCESS);
}

int DrawColoredText(Canvas *c, const Font &font, int x, int y, Color &color,
                    const Color *background_color, const char *utf8_text,
                    int extra_spacing) {
  const int start_x = x;
  char new_color[255] = {};
  bool in_control_mode = false;
  int i;
  while (*utf8_text) {
    const uint32_t cp = utf8_next_codepoint(utf8_text);
    if (cp == 60) {
      i = 0;
      memset(new_color, 0, 255);
      in_control_mode = true;
      continue;
    }
    if (cp == 62) {
      parseColor(&color, new_color);
      printf("Switching color: '%s'\n", new_color);
      in_control_mode = false;
      continue;
    }
    if (in_control_mode) {
      new_color[i] = char(cp);
      i += 1;
      continue;
    }
    x += font.DrawGlyph(c, x, y, color, background_color, cp);
    x += extra_spacing;
  }
  return x - start_x;
}

int main(int argc, char *argv[]) {

  struct sigaction sa;
  sigset_t newset;

  sigemptyset(&newset);
  sigaddset(&newset, SIGHUP);
  sigprocmask(SIG_BLOCK, &newset, 0);
  sa.sa_handler = termHandler;
  sigaction(SIGTERM, &sa, 0);

  RGBMatrix::Options matrix_options;
  rgb_matrix::RuntimeOptions runtime_opt;
  if (!rgb_matrix::ParseOptionsFromFlags(&argc, &argv, &matrix_options,
                                         &runtime_opt)) {
    return usage(argv[0]);
  }

  Color color(255, 255, 0);
  Color bg_color(0, 0, 0);
  Color flood_color(0, 0, 0);
  Color outline_color(0, 0, 0);
  bool with_outline = false;

  const char *bdf_font_file = NULL;
  int x_orig = 0;
  int y_orig = 0;
  int letter_spacing = 0;

  int opt;
  while ((opt = getopt(argc, argv, "x:y:f:C:B:O:S:F:")) != -1) {
    switch (opt) {
    case 'x':
      x_orig = atoi(optarg);
      break;
    case 'y':
      y_orig = atoi(optarg);
      break;
    case 'f':
      bdf_font_file = strdup(optarg);
      break;
    case 'S':
      letter_spacing = atoi(optarg);
      break;
    case 'C':
      if (!parseColor(&color, optarg)) {
        fprintf(stderr, "Invalid color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'B':
      if (!parseColor(&bg_color, optarg)) {
        fprintf(stderr, "Invalid background color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'O':
      if (!parseColor(&outline_color, optarg)) {
        fprintf(stderr, "Invalid outline color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      with_outline = true;
      break;
    case 'F':
      if (!parseColor(&flood_color, optarg)) {
        fprintf(stderr, "Invalid background color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    default:
      return usage(argv[0]);
    }
  }

  if (bdf_font_file == NULL) {
    fprintf(stderr, "Need to specify BDF font-file with -f\n");
    return usage(argv[0]);
  }

  /*
   * Load font. This needs to be a filename with a bdf bitmap font.
   */
  rgb_matrix::Font font;
  if (!font.LoadFont(bdf_font_file)) {
    fprintf(stderr, "Couldn't load font '%s'\n", bdf_font_file);
    return 1;
  }

  /*
   * If we want an outline around the font, we create a new font with
   * the original font as a template that is just an outline font.
   */
  rgb_matrix::Font *outline_font = NULL;
  if (with_outline) {
    outline_font = font.CreateOutlineFont();
  }

  RGBMatrix *canvas = RGBMatrix::CreateFromOptions(matrix_options, runtime_opt);
  if (canvas == NULL) {
    return 1;
  }

  const bool all_extreme_colors =
      (matrix_options.brightness == 100) && FullSaturation(color) &&
      FullSaturation(bg_color) && FullSaturation(outline_color);
  if (all_extreme_colors) {
    canvas->SetPWMBits(1);
  }

  const int x = x_orig;
  int y = y_orig;

  int sockfd;
  struct sockaddr_in servaddr, cliaddr;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  memset(&servaddr, 0, sizeof(servaddr));
  memset(&cliaddr, 0, sizeof(cliaddr));

  // Filling server information
  servaddr.sin_family = AF_INET; // IPv4
  servaddr.sin_addr.s_addr = INADDR_ANY;
  servaddr.sin_port = htons(PORT);

  // Bind the socket with the server address
  if (bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  printf("Server started\n");

  canvas->Fill(flood_color.r, flood_color.g, flood_color.b);

  const char *pre = "<line:";

  int maxLines = floor(matrix_options.rows - y_orig) / font.height();

  while (StopFlag) {
    char line[1024] = {};
    socklen_t cs = sizeof(cliaddr);
    printf("INFO Waiting for a UDP datagram...\n");
    int rc = recvfrom(sockfd, line, sizeof(line), 0,
                      (struct sockaddr *)&cliaddr, &cs);

    if (rc < 0) {
      printf("ERROR READING FROM SOCKET!");
    } else {
      printf("Client : %s\n", line);
      bool line_empty = strlen(line) == 0;

      if (strncmp(pre, line, strlen(pre)) == 0) {
        char new_line[1024] = {};
        int i;
        int j = 0;
        bool done = false;

        for (i = 0; line[i] != 0; i++) {
          new_line[j] = line[i];
          j += 1;
          if (done) {
            continue;
          }
          if (line[i] == 58) {
            memset(new_line, 0, 1024);
            j = 0;
          }
          if (line[i] == 62) {
            printf("Got >\n");
            y = ((atoi(new_line) - 1) % maxLines);
            printf("Moved to line %i", y);
            y = y * font.height();
            memset(new_line, 0, 1024);
            j = 0;
            done = true;
          }
        }

        strcpy(line, new_line);
      }

      if ((y + font.height() > canvas->height()) || line_empty) {
        canvas->Fill(flood_color.r, flood_color.g, flood_color.b);
        y = y_orig;
      }

      if (line_empty) {
        printf("Line empty, emptied canvas\n");
        continue;
      }

      // if (outline_font) {
      //   // The outline font, we need to write with a negative (-2)
      //   // text - spacing,
      //   // as we want to have the same letter pitch as the regular text that
      //   // we then write on top.
      //   rgb_matrix::DrawText(canvas, *outline_font, x - 1, y +
      //   font.baseline(),
      //                        outline_color, &bg_color, line,
      //                        letter_spacing - 2);
      // }
      // The regular text. Unless we already have filled the background with
      // the outline font, we also fill the background here.
      DrawColoredText(canvas, font, x, y + font.baseline(), color, &bg_color,
                      line, letter_spacing);
      y += font.height();
    }
  }

  // Finished. Shut down the RGB matrix.
  canvas->Clear();
  delete canvas;

  return 0;
}
