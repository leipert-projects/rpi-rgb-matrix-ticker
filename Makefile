include .env
export HARDWARE_DESC
export USER_DEFINES
export PYTHON

BIN_DIR=bin

BIN=$(BIN_DIR)/server
SRC = $(wildcard src/*.cc)
OBJECTS = $(patsubst src/%.cc, bin/%.o, $(SRC))

RGB_INCDIR=rpi-rgb-led-matrix/include
RGB_LIBDIR=rpi-rgb-led-matrix/lib
RGB_LIBRARY_NAME=rgbmatrix
RGB_LIBRARY=$(RGB_LIBDIR)/lib$(RGB_LIBRARY_NAME).a

CFLAGS=-Wall -O3 -g -Wextra -Wno-unused-parameter
CXXFLAGS=$(CFLAGS)
LDFLAGS+=-L$(RGB_LIBDIR) -l$(RGB_LIBRARY_NAME) -lrt -lm -lpthread

all : $(BIN)

$(BIN) : $(RGB_LIBRARY) $(OBJECTS)
	$(CXX) $(CFLAGS) $(OBJECTS) -o $@ $(LDFLAGS)

$(RGB_LIBRARY): FORCE
	$(MAKE) -C $(RGB_LIBDIR)

$(BIN_DIR)/%.o : src/%.cc | $(BIN_DIR)
	$(CXX) $(CFLAGS) -I$(RGB_INCDIR) -c -o $@ $<

$(BIN_DIR):
	mkdir $@

FORCE:
.PHONY: FORCE