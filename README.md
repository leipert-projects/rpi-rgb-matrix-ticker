# rpi-rgb-matrix-ticker

> Simple UDP Server that receives lines and draws them on a LED matrix.

## How does this work?

1. rpi-rgb-matrix-ticker runs on a Raspberry Pi
   - Connected to a Network
   - The Raspberry Pi is connected to an LED panel
2. Send lines of text via UDP. The text is rendered on the Panel.
   The text can be send via Network or locally.

   Personally I send updates via Node-RED which is the easiest way for me to manipulate data.
   I have Node-RED running on a seperate device already, but if this is your first project,
   consider running the ticker and node-red 

## What Hardware do I need?

I tested this with the following hardware:

- Raspberry Pi

## Setup instructions

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

